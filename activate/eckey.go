package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"math/big"
)

var curve = elliptic.P256

type LocalKey ecdsa.PrivateKey

func (this *LocalKey) Create() *LocalKey {
	key, err := ecdsa.GenerateKey(curve(), rand.Reader)
	if err != nil {
		return nil
	}
	return (*LocalKey)(key)
}

func (this *LocalKey) Public() *ForeignKey {
	return (*ForeignKey)(&this.PublicKey)
}

func pad(b []byte, length int) []byte {
	if len(b) >= length {
		return b
	}

	padded := make([]byte, length)
	copy(padded[length-len(b):], b)
	return padded
}

func (this *LocalKey) Bytes() []byte {
	return pad(this.D.Bytes(), this.Public().KeyLength())
}

func (this *LocalKey) Import(hexstr string) (*LocalKey, error) {
	if this == nil {
		this = &LocalKey{}
	}

	if b, err := hex.DecodeString(hexstr); err != nil {
		return nil, err
	} else {
		this.D = new(big.Int).SetBytes(b)
	}

	this.PublicKey.Curve = elliptic.P256()
	this.PublicKey.X, this.PublicKey.Y = this.PublicKey.Curve.ScalarBaseMult(this.D.Bytes())
	return this, nil
}

func (this *LocalKey) String() string {
	return hex.EncodeToString(this.Bytes())
}

func sha256sum(data []byte) []byte {
	sum := sha256.Sum256(data)
	return sum[:]
}

func (this *LocalKey) SignNoASN1(data []byte) ([]byte, error) {
	digest := sha256sum(data)
	//log.Printf("digest = %s\n", hex.EncodeToString(digest))
	r, s, err := ecdsa.Sign(rand.Reader, (*ecdsa.PrivateKey)(this), digest)
	if err != nil {
		return nil, err
	}
	n := this.Public().KeyLength()
	return append(pad(r.Bytes(), n), pad(s.Bytes(), n)...), nil
}

func (this *LocalKey) CsrToCert(pubkey *ForeignKey, tags string) ([]byte, error) {
	pub := pubkey.Bytes()
	n := this.Public().KeyLength()

	// cert format: length(2) + version(1) + reserved(1) + sig(64) + pubkey(64) + tags
	cert := make([]byte, 4+n*2+len(pub)+len(tags))

	// tags
	i := len(cert) - len(tags)
	copy(cert[i:], tags)

	// pubkey
	i -= len(pub)
	copy(cert[i:], pub)

	// sig
	if b, err := this.SignNoASN1(cert[i:]); err != nil {
		return nil, err
	} else {
		i -= 2 * n
		copy(cert[i:], b)
	}

	// version
	i -= 2
	cert[i] = 0

	// length
	m := uint16(len(cert[i:]))
	i -= 2
	binary.LittleEndian.PutUint16(cert, m)

	return cert, nil
}

type ForeignKey ecdsa.PublicKey

func (this *ForeignKey) KeyLength() int {
	return (this.Curve.Params().N.BitLen() + 7) / 8
}

func (this *ForeignKey) Bytes() []byte {
	n := this.KeyLength()
	v := pad(this.X.Bytes(), n)
	return append(v, pad(this.Y.Bytes(), n)...)
}

func (this *ForeignKey) String() string {
	return hex.EncodeToString(this.Bytes())
}
