package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/flynn/hid"
)

const (
	CMD_CHANNEL    = 3
	HID_BLOCK_SIZE = 64
)

const (
	CMD_CONTINUE         = 0
	CMD_PROTOCOL_VERSION = 1
	CMD_WRITE_FIFO       = 4
	CMD_AUTHORIZE        = 10
	CMD_PRODUCT_INFO     = 11
	CMD_REBOOT           = 12
	CMD_WRITE_CREDENTIAL = 13
)

type Session struct {
	Dev       hid.Device
	CmdBuffer []byte
	Input     <-chan []byte
}

func CreateSession(di *hid.DeviceInfo) (*Session, error) {
	if dev, err := di.Open(); err != nil {
		return nil, err
	} else {
		this := &Session{
			Dev:       dev,
			CmdBuffer: make([]byte, di.OutputReportLength+1),
			Input:     dev.ReadCh(),
		}
		return this, nil
	}
}

func (this *Session) Close() {
	this.Dev.Close()
}

func (this *Session) Exec(cmd, p1, p2 byte, data []byte) ([]byte, error) {
	this.Prepare(cmd, p1, p2, len(data))
	if err := this.Send(data); err != nil {
		return nil, err
	}
	return this.GetResponse()
}

func (this *Session) Prepare(cmd, p1, p2 byte, size int) {
	this.CmdBuffer[0] = CMD_CHANNEL
	this.CmdBuffer[1] = 0
	this.CmdBuffer[2] = cmd
	this.CmdBuffer[3] = p1
	this.CmdBuffer[4] = p2
	this.CmdBuffer[7] = byte(size & 0xFF)
	this.CmdBuffer[6] = byte((size >> 8) & 0xFF)
	this.CmdBuffer[5] = byte((size >> 16) & 0xFF)
}

func min(x, y int) int {
	if x <= y {
		return x
	} else {
		return y
	}
}

func (this *Session) Send(data []byte) error {
	if data == nil {
		data = []byte{}
	}
	m := len(this.CmdBuffer) - 8
	sent := false
	remains := len(data)
	n := min(remains, m)
	for remains > 0 || !sent {
		time.Sleep(100 * time.Microsecond)
		copy(this.CmdBuffer[8:], data[:n])

		if err := this.Dev.Write(this.CmdBuffer); err != nil {
			return err
		}
		sent = true
		remains -= n
		if remains > 0 {
			data = data[n:]
			n = min(remains, m)
			this.Prepare(CMD_CONTINUE, 0, 0, n)
		}
	}

	//log.Printf("Command sent.\n")
	return nil
}

func (this *Session) GetResponse() ([]byte, error) {
	// receive first packet
	resp := <-this.Input
	if len(resp) < 8 {
		return nil, fmt.Errorf("Response too short")
	} else if resp[0] != CMD_CHANNEL {
		return nil, fmt.Errorf("Response channel error")
	}
	code := uint16(resp[3])<<8 | uint16(resp[4])
	size := uint16(resp[6])<<8 | uint16(resp[7])
	msg := resp[8:]
	if len(msg) > int(size) {
		msg = msg[:size]
	}

	for len(msg) < int(size) {
		resp := <-this.Input
		if resp[0] != CMD_CHANNEL {
			return nil, fmt.Errorf("Response channel error")
		} else if resp[2] != 0 {
			return nil, fmt.Errorf("Response format error")
		}
		n := uint16(resp[6])<<8 | uint16(resp[7])
		if n == 0 {
			n = uint16(len(resp) - 8)
		}
		msg = append(msg, resp[8:(8+n)]...)
	}
	//log.Printf("response = %03d %s\n", code, string(msg))

	if code != 200 {
		return nil, fmt.Errorf("%03d %s", code, string(msg))
	} else {
		return msg, nil
	}
}

func (this *Session) Authorize(on bool) error {
	if !on {
		_, err := this.Exec(CMD_AUTHORIZE, 0, 0, nil)
		return err
	}

	if b, err := this.Exec(CMD_AUTHORIZE, 1, 0, nil); err != nil {
		return err
	} else if b, err = hex.DecodeString(string(b)); err != nil {
		return err
	} else {
		repeat := int(b[0]) & 0x7
		data := make([]byte, len(b)*(repeat+1))
		for ; repeat >= 0; repeat-- {
			copy(data[len(b)*repeat:], b)
		}

		if b, err = CA.SignNoASN1(data); err != nil {
			return err
		} else if b, err = this.Exec(CMD_AUTHORIZE, 2, 0, b); err != nil {
			return err
		} else {
			return nil
		}
	}
}

func (this *Session) Info() (map[string]string, error) {
	b, err := this.Exec(CMD_PRODUCT_INFO, 0, 0, nil)
	if err != nil {
		return nil, err
	}
	log.Printf("info = %s\n", string(b))

	var m map[string]string
	if err := json.Unmarshal(b, &m); err != nil {
		return nil, err
	}
	return m, nil
}

func (this *Session) WriteCredential(tags string) error {
	var key *LocalKey
	if key = key.Create(); key == nil {
		return fmt.Errorf("Fail to create key")
	}

	cert, err := CA.CsrToCert(key.Public(), tags)
	if err != nil {
		return fmt.Errorf("Error generating cert: %v\n", err)
	}

	//log.Printf("key = %s\n", key.String())
	if _, err = this.Exec(CMD_WRITE_CREDENTIAL, 0, 0, key.Bytes()); err != nil {
		return err
	}

	log.Printf("Cert[%d] = %s\n", len(cert), hex.EncodeToString(cert))
	_, err = this.Exec(CMD_WRITE_CREDENTIAL, 1, 0, cert)
	return err
}
