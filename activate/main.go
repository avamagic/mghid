package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/flynn/hid"
)

var CA *LocalKey

func main() {
	newcaArg := flag.Bool("newkey", false, "Create new private key")
	forceArg := flag.Bool("force", false, "Force activate even if already activated")
	caArg := flag.String("ca", "", "CA private key")
	flag.Parse()

	if *newcaArg {
		fmt.Printf("%s\n", CA.Create().String())
		return
	}

	var err error
	if len(*caArg) > 0 {
		if CA, err = CA.Import(*caArg); err != nil {
			log.Fatalf("Failed to load CA: %v\n", err)
		}
		log.Printf("CA public = %s.\n", CA.Public().String())
	}
	if CA == nil {
		log.Fatalf("No CA defined.\n")
	}

	devices, err := hid.Devices()
	if err != nil || len(devices) == 0 {
		log.Fatalf("No HID devices connected or usb error. (%v)\n", err)
	}

	var di *hid.DeviceInfo
	for _, d := range devices {
		//log.Printf("Detected %04x:%04x/%04x/%04x ...", d.VendorID, d.ProductID, d.UsagePage, d.Usage)
		if d.VendorID == 0x1B80 && d.ProductID == 0xE10E && d.UsagePage == 0x06 && d.Usage == 0x01 {
			log.Printf("Found device: %04x:%04x/%04x/%04x", d.VendorID, d.ProductID, d.UsagePage, d.Usage)
			di = d
			break
		}
	}
	if di == nil {
		log.Fatalf("Device not found.\n")
	}

	sess, err := CreateSession(di)
	if err != nil {
		log.Fatalf("Failed to open device: %v\n", err)
	}
	defer func() {
		sess.Authorize(false)
		sess.Close()
	}()

	if err := sess.Authorize(true); err != nil {
		log.Fatalf("No access right: %v\n", err)
	}

	var tags string
	if info, err := sess.Info(); err != nil {
		log.Fatalf("Failed to get device info: %v\n", err)
	} else {
		if !*forceArg && info["credential"] == "present" {
			log.Printf("Device is already activated.\n")
			return
		}

		if len(info["serial"]) == 0 || info["serial"] == "00000000" {
			log.Printf("Device has no serial number.")
			return
		}
		tags = fmt.Sprintf("%s", info["serial"])
	}

	if err := sess.WriteCredential(tags); err != nil {
		log.Printf("Fail to burn key/cert: %v\n", err)
	} else {
		log.Printf("Ok")
	}
}
