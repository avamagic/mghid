#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "crypto/micro-ecc/uECC.h"
#include "crypto/sha256/sha256.h"

#ifndef NUM_ECC_BYTES
#define NUM_ECC_BYTES           32
#endif

#define PARTSIZE_PRIVKEYS       (256)
#define NUM_PRIVKEYS            (PARTSIZE_PRIVKEYS/NUM_ECC_BYTES)

__no_init uint8_t               privKeys[NUM_PRIVKEYS][NUM_ECC_BYTES]   @ (DATA_EEPROM_BANK2_END+1-sizeof(device_keycert_t)-PARTSIZE_PRIVKEYS); // 256B from the bottom

void eeprom_copy(uint32_t dst, uint32_t src, size_t length) {
  HAL_FLASHEx_DATAEEPROM_Unlock();
  for (;length>0;length-=sizeof(uint32_t),dst+=sizeof(uint32_t),src+=sizeof(uint32_t)) {
    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, dst, *(uint32_t *)src);      // *dst=*src
  }
  HAL_FLASHEx_DATAEEPROM_Lock();
}

static int findId(char *s) {
  for (; *s<'0' || *s>'9'; s++)
    ;
  return atoi(s);
}

static uint8_t unhex(char ch) {
  uint8_t v = ch-'0';
  if (v<10) return v;

  v = ch-'A';
  if (v<6) return v + 10;

  v = ch-'a';
  if (v<6) return v + 10;

  return 0;
}

static void hex2bin(uint8_t *dst, size_t size, uint8_t *src) {
  for (; size>0 && *src && *(src+1); size--, dst++) {
    *dst = unhex(*src++) << 4 | unhex(*src++);
  }
  if (size>0) memset(dst, 0, size);
}

static uint8_t vars[3][NUM_ECC_BYTES*2];

static uint8_t *decodeVar(char *label) {
  if (strchr("$@%", label[0])!=NULL) {
    int i = findId(label);
    return (i>=0 && i<LENGTH(vars)) ? vars[i] : NULL;
  }

  // Warning: inplace convert
  size_t n = strlen(label);
  hex2bin((uint8_t *)label, n/2, (uint8_t *)label);
  return (uint8_t *)label;
}

int crypto_set_variable(int argc, char *argv[]) {
  if (argc<3)
    return BAD_REQUEST;

  int slot = findId(argv[1]);
  if (slot<0 || slot>=LENGTH(vars))
    return BAD_REQUEST;

  hex2bin(vars[slot], sizeof(vars[slot]), (uint8_t *)argv[2]);
  return OK;
}

bool isAllZero(uint8_t *s, size_t size) {
  for (uint32_t *p=(uint32_t *)s; p<(uint32_t *)(s+size); p+=sizeof(uint32_t)) {
    if (*p) return false;
  }
  return true;
}

static uint8_t *labelToPrivKey(char *label) {
  int i = findId(label);

  if (i==0)  // device key
    return devKeyCert.key;

  if (i>0 && i<=LENGTH(privKeys)) {
    uint8_t *key = privKeys[i-1];
    return !isAllZero(key, NUM_ECC_BYTES) ? key : NULL;
  }

  return NULL;
}

int crypto_generate(int argc, char *argv[]) {
  if (argc<2) return UNPROCESSABLE;

  int i = findId(argv[1]);
  if (i<1 || i>LENGTH(privKeys)) return UNPROCESSABLE;
  uint8_t *key = privKeys[i-1];

  uint8_t priv[NUM_ECC_BYTES];
  uint8_t pub[NUM_ECC_BYTES*2];
  if (!uECC_make_key(pub, priv, uECC_secp256r1()))
      return UNPROCESSABLE;

  eeprom_copy((uint32_t)key, (uint32_t)priv, sizeof(priv));
  memset(priv, 0xFF, NUM_ECC_BYTES);

  dump("200", pub, sizeof(pub));
  return 0;
}

int crypto_get_public(int argc, char *argv[]) {
  uint8_t *key = devKeyCert.key;
  if (argc>1) {
    key = labelToPrivKey(argv[1]);
    if (key==NULL) return UNPROCESSABLE;
  }

  uint8_t pub[NUM_ECC_BYTES*2];
  if (!uECC_compute_public_key(key, pub, uECC_secp256r1()))
    return UNPROCESSABLE;
  dump("200", pub, sizeof(pub));
  return 0;
}

int crypto_get_cert(int argc, char *argv[]) {
  uint8_t *key = devKeyCert.key;
  if (argc>1) {
    key = labelToPrivKey(argv[1]);
    if (key!=devKeyCert.key) return UNPROCESSABLE;
  }

  uint16_t size = *(uint16_t *)devKeyCert.cert;
  size+=2;
  if (size>sizeof(devKeyCert.cert)) size=sizeof(devKeyCert.cert);

  dump("200", devKeyCert.cert, size);
  return 0;
}

int crypto_sign(int argc, char *argv[]) {
  uint8_t *key = devKeyCert.key;
  if (argc>1) {
    key = labelToPrivKey(argv[1]);
    if (key==NULL) return UNPROCESSABLE;
  }

  uint8_t *digest = vars[0];
  if (argc>2) {
    digest = decodeVar(argv[2]);
    if (digest==NULL) return UNPROCESSABLE;
  }

  uint8_t sig[NUM_ECC_BYTES*2];
  if (!uECC_sign(key, digest, NUM_ECC_BYTES, sig, uECC_secp256r1()))
    return UNPROCESSABLE;
  dump("200", sig, sizeof(sig));
  memset(sig, 0xff, sizeof(sig));
  return 0;
}

int crypto_agree(int argc, char *argv[]) {
  uint8_t *key = devKeyCert.key;
  if (argc>1) {
    key = labelToPrivKey(argv[1]);
    if (key==NULL) return UNPROCESSABLE;
  }

  uint8_t *pub = vars[0];
  if (argc>2) {
    pub = decodeVar(argv[2]);
    if (pub==NULL) return UNPROCESSABLE;
  }

  uint8_t secret[NUM_ECC_BYTES];
  if (!uECC_shared_secret(pub, key, secret, uECC_secp256r1()))
    return UNPROCESSABLE;

  // apply KDF
  SHA256_CTX ctx;
  sha256_init(&ctx);
  sha256_update(&ctx, secret, sizeof(secret));
  sha256_final(&ctx, secret);

  dump("200", secret, sizeof(secret));
  memset(secret, 0xff, sizeof(secret));
  return 0;
}


int crypto_verify(int argc, char *argv[]) {
  uint8_t *key = devKeyCert.key;
  if (argc>1) {
    key = labelToPrivKey(argv[1]);
    if (key==NULL) return UNPROCESSABLE;
  }

  uint8_t *sig = vars[0];
  if (argc>2) {
    sig = decodeVar(argv[2]);
    if (sig==NULL) return UNPROCESSABLE;
  }

  uint8_t *dgst = vars[1];
  if (argc>3) {
    dgst = decodeVar(argv[3]);
    if (dgst==NULL) return UNPROCESSABLE;
  }

  uint8_t *pub = vars[2];
  if (argc>4) {
    pub = decodeVar(argv[4]);
    if (pub==NULL) return UNPROCESSABLE;
  }

  if (!uECC_verify(pub, dgst, 32, sig, uECC_secp256r1()))
    return UNPROCESSABLE;
  return OK;
}
