#include "main.h"
#include <memory.h>

typedef PACKED_STRUCT {
  uint16_t size, count;
  uint16_t head;
  uint8_t buf[1];
} cycbuf_t;

void *cycbuf_setup(uint8_t *buffer, int size) {
  cycbuf_t *this = (cycbuf_t *)buffer;

  memset(buffer, 0, size);
  this->size = size - (sizeof(cycbuf_t)-1);
  return this;
}

int cycbuf_size(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  return (int)this->size;
}

int cycbuf_count(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  return (int)this->count;
}

int cycbuf_is_empty(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  return (this->count==0);
}

int cycbuf_is_full(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  return (this->count==this->size);
}

static uint8_t *cycbuf_content_head(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  return this->buf + this->head;
}

static int cycbuf_content_block_size(void *h) {
  cycbuf_t *this = (cycbuf_t *)h;

  uint16_t n = this->size-this->head;
  if (n>this->count) n = this->count;
  return (int)n;
}

static int cycbuf_content_consume(void *h, int n) {
  cycbuf_t *this = (cycbuf_t *)h;

  if (n<=0) return 0;
  if (n>this->count) n = this->count;

  this->head += (uint16_t)n;
  this->head %= this->size;
  this->count -= (uint16_t)n;
  return n;
}

void cycbuf_dump_all(void *h, int (*dump_block)(uint8_t *src, int n)) {
  int n, m;

  while( (n=cycbuf_content_block_size(h)) >0 ) {
    uint8_t *buf = cycbuf_content_head(h);
    m = dump_block(buf, n);
    if (m>0) cycbuf_content_consume(h, m);
    if (m<n) return;
  }
}

int cycbuf_read(void *h, void *buf, int size) {
  if (size<=0 || cycbuf_is_empty(h)) return 0;

  // avail = min(buf_end-head, size, count);
  int n = cycbuf_content_block_size(h);
  if (n>size) n=size;

  memcpy(buf, cycbuf_content_head(h), n);
  n = cycbuf_content_consume(h, n);

  return (int)n + cycbuf_read(h, (uint8_t *)buf + n, size-n);
}

int cycbuf_write(void *h, void *buf, int size) {
  if (size<=0 || cycbuf_is_full(h)) return 0;
  cycbuf_t *this = (cycbuf_t *)h;

  uint16_t tail = this->head + this->count;
  uint16_t n = this->size - this->count;
  if ( tail < this->size ) { // data in the middle of the buffer
    n -= this->head;
  } else { // data in the two sides of the buffer
    tail -= this->size;
  }
  if (n>size) n = size;

  memcpy(this->buf+tail, buf, (unsigned int)n);
  this->count += n;
  return (int)n + cycbuf_write(h, (uint8_t *)buf + n, size-n);
}

#if 0
#include <stdio.h>
#include <string.h>

typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

#ifdef __IAR_SYSTEMS_ICC__
#define PACKED_STRUCT __packed struct
#else
#define PACKED_STRUCT struct __attribute__((packed))
#endif

int test_main() {
  uint8_t cmdbuf[1024];
  uint8_t cycbuf[12];

  void *h = cycbuf_setup(cycbuf, sizeof(cycbuf));
  char *cmd, *arg;
  int n;

  while(1) {
    printf("\ncycbuf(%d/%d)> ", cycbuf_count(h), cycbuf_size(h)); fflush(stdout);
    gets(cmdbuf);
    cmd = strtok(cmdbuf, " \t\r\n");
    if (!cmd || strlen(cmd)==0) continue;
    arg = strtok(NULL, "\r\n");

    switch(cmd[0]) {
    case 'q':
      printf("Bye.\n"); fflush(stdout);
      return 0;

    case 'p':
      printf("buffer content: ");
      for(int i=0; i<(sizeof(cycbuf)-6); i++) {
        uint8_t ch = cycbuf[i+6];
        putchar(ch>=' ' && ch<='~' ? ch : '-');
      }
      putchar('\n');

    case 'w':
      if (!arg || strlen(arg)==0) {
        printf("No arguments\n");
        continue;
      }
      n = cycbuf_write(h, arg, strlen(arg));
      printf("%d bytes written.\n", n);
      break;

    case 'r':
      n = sizeof(cmdbuf);
      if (arg && strlen(arg)>0) {
        sscanf(arg, "%d", &n);
      }
      n = cycbuf_read(h, cmdbuf, n);
      printf("%d bytes read: ", n);
      for(int i=0; i<n; i++) putchar(cmdbuf[i]);
      putchar('\n');
      break;
    }
  }
}
#endif
