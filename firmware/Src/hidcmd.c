#include "main.h"
#include <stdbool.h>
#include "buildate.h"
#include "usbd_hid.h"
#include "crypto/sha256/sha256.h"
#include "crypto/micro-ecc/uECC.h"

#define DEBUG(x)        //printf x
#define DUMP(x)         //dump x

enum {
  CONTINUE = 0,
  PROTOCOL_VERSION,
  WRITE_FIFO = 4,

  AUTHORIZE = 10,
  PRODUCT_INFO,
  REBOOT,
  WRITE_CREDENTIAL
};

typedef PACKED_STRUCT {
    uint8_t cla;        // class, must be 0 for u2f
    uint8_t ins;        // instruction
    uint8_t p1;         // parameter 1
    uint8_t p2;         // parameter 2
    uint8_t LC1;        // length
    uint8_t LC2;        // length
    uint8_t LC3;        // length
    uint8_t payload[1];
} apdu_t;

static int onHidRequest(uint8_t *buffer, int size);
int (*hidreq_handler)(uint8_t *buf, int size) = onHidRequest;

/*
 * Slice data
 */
typedef PACKED_STRUCT slice_data_s {
  int (*handle_slice)(struct slice_data_s *, uint8_t *, int);
  uint16_t elapsed, expecting;
  uint8_t command, error;
} slice_data_t;

static slice_data_t pending;

static int onSliceBegin(uint8_t *buffer, int size, int (*handler)(slice_data_t *, uint8_t *, int) ) {
  apdu_t *req = (apdu_t *)(buffer+1);
  uint16_t len = ((uint16_t)req->LC3 & 0xFF) | ((uint16_t)req->LC2 << 8); // | ((int)req->LC1 << 16);

  pending.command = req->ins;
  pending.expecting = len;
  pending.elapsed = 0;
  pending.error = 0;
  pending.handle_slice = handler;
  req->ins = CONTINUE;
  return onHidRequest(buffer, size);
}

static int onFifoDataArrived(slice_data_t *this, uint8_t *data, int len) {
  return cycbuf_write(fifoBuf, data, len)!=len;
}

extern void eeprom_copy(uint32_t dst, uint32_t src, size_t length);
static int onCertDataReceived(slice_data_t *this, uint8_t *data, int len) {
  uint32_t src = (uint32_t)data;
  uint32_t dst = (uint32_t)(devKeyCert.cert) + this->elapsed;
  //dump("onCertDataReceived: ", data, len);

  if (src%4!=0 || dst%4!=0 || len%4!=0) return 1;
  eeprom_copy(dst, src, len);
  return 0;
}

/*
 * Authorization
 */

typedef union {
  uint8_t sig[64];
  uint32_t status;
  PACKED_STRUCT {
    uint8_t reserved[56];
    uint8_t text[8];
  } challenge;
} authrec_t;

static authrec_t authrec = {
  .status = 0x1234
};

static bool isAuthorized() {
  return authrec.status == 0;
}

static void setAuthorized(bool on) {
  authrec.status = on ? 0 : 0x1234;
}

typedef union {
  uint8_t value;
  struct {
    uint8_t repeat: 3;
    uint8_t flip: 1;
    uint8_t reserved: 4;
  };
} challenge_flag_t;

static int onAuthResponseArrived(slice_data_t *this, uint8_t *data, int len) {
  static uint8_t caPub[64] = "\xa2\x95\x07\xd3\x63\xe5\x70\xd2\xb7\x43\xe9\xbb\x55\x09\x2a\xf7\x41\x20\x80\xbe\x5a\xd0\x19\x12\x24\xd8\x9e\x6a\xef\x12\x6d\xf4\x5e\x35\x59\xf6\x5e\x2c\x76\x2f\xcd\x4e\x15\x71\x86\x51\x99\x49\xba\x55\x46\x9b\xb1\x8c\x9b\x44\xf3\x5b\xe7\xb8\x50\xd4\x51\x23";

  uint8_t challenge[8];
  memcpy(challenge, authrec.challenge.text, sizeof(challenge)); /// backup challenge to stack
  DUMP(("onAuthResponseArrived: challenge = ", challenge, sizeof(challenge)));
  memcpy(authrec.sig + this->elapsed, data, len);
  if (this->expecting-len>0) return 0;

  // last packet
  SHA256_CTX ctx;
  challenge_flag_t flag = {
    .value = *challenge
  };
  DEBUG(("challenge flag = %x, repeat = %d\n", flag.value, (uint8_t)flag.repeat));

  sha256_init(&ctx);
  sha256_update(&ctx, challenge, sizeof(challenge));
  for (uint8_t i=(uint8_t)flag.repeat; i>0; i--)
    sha256_update(&ctx, challenge, sizeof(challenge));

  uint8_t *dgst = data; // reuse buffer
  sha256_final(&ctx, dgst);
  DUMP(("digest = ", dgst, 32));

  if (!uECC_verify(caPub, dgst, 32, authrec.sig, uECC_secp256r1())) {
    DEBUG(("uECC_verify: not match\n"));
    return 2;
  } else {
    DEBUG(("uECC_verify: Ok\n"));
    setAuthorized(true);
    return 0;
  }
}

/*
 * request and response
 */

#define MAX_PAYLOAD     (HID_BLOCK_SIZE-1-sizeof(apdu_t))

static void reply(uint8_t cmd, uint16_t status, uint8_t *msg) {
  static uint8_t buffer[HID_BLOCK_SIZE] = { 0x3 };
  static apdu_t *resp = (apdu_t *)(buffer+1);
  extern USBD_HandleTypeDef hUsbDeviceFS;
  USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef*)hUsbDeviceFS.pClassData;

  while(hhid->state != HID_IDLE)
    HAL_Delay(1);

  resp->cla = 0;
  resp->ins = cmd;
  resp->p2 = status & 0xFF;
  resp->p1 = (status >> 8) & 0xFF;

  int len = strlen(msg);

  resp->LC3 = len & 0xFF;
  resp->LC2 = (len >> 8) & 0xFF;
  resp->LC1 = (len >> 16) & 0xFF;

  int n=min(len, MAX_PAYLOAD);
  do {
    memset(resp->payload, 0, buffer+sizeof(buffer)-resp->payload);
    if (n>0) memcpy(resp->payload, msg, n);
    USBD_HID_SendReport(&hUsbDeviceFS, buffer, sizeof(buffer));
    len -= n, msg += n;
    while(hhid->state != HID_IDLE) HAL_Delay(1); // blocking until sent
    if (len<=0) break; // end of blocks

    // next block
    memset(resp, 0, sizeof(*resp)); // prepare for next block
    n = min(len, MAX_PAYLOAD);
    resp->LC3 = n & 0xFF;
    resp->LC2 = (n >> 8) & 0xFF;
    resp->LC1 = (n >> 16) & 0xFF;
  } while(len>0);
}

static int onHidRequest(uint8_t *buffer, int size) {
  apdu_t *req = (apdu_t *)(buffer+1);

  DUMP(("handle_hidreq:", (uint8_t *)req, size-1));
  if (req->cla != 0) {
    reply(req->ins, 400, "Bad request");
    return 0;
  }
  uint16_t len = ((uint16_t)req->LC3 & 0xFF) | ((uint16_t)req->LC2 << 8); // | ((int)req->LC1 << 16);

  if (!isAuthorized() && req->ins>AUTHORIZE) {
    reply(req->ins, 401, "Unauthorized");
    return 0;
  }

  uint8_t *msg = "Ok";
  uint16_t status = 200;

  switch(req->ins) {
  case CONTINUE:
    size -= sizeof(apdu_t);
    if (size>(int)pending.expecting) size = (int)pending.expecting;

    if (pending.handle_slice) {
      pending.error |= pending.handle_slice(&pending, req->payload, size);
    }
    pending.elapsed += (uint16_t)size;
    pending.expecting -= (uint16_t)size;
    if (pending.expecting>0)
      return 0;

    // last packet
    req->ins = pending.command;
    if (pending.error) {
      status = 405;
      msg = "Input data error";
    }
    break;

  case PROTOCOL_VERSION:
    msg = "1";
    break;

  case AUTHORIZE:
    switch(req->p1) {
    case 0: // authorize off
      setAuthorized(false);
      break;

    case 1: // authorize request
      extern int rand_bytes(uint8_t * dst, uint32_t sz);
      rand_bytes(authrec.challenge.text, sizeof(authrec.challenge.text));
      msg = (uint8_t *)(req+1);
      for (int i=0; i<sizeof(authrec.challenge.text); i++)
        sprintf(msg+2*i, "%02x", authrec.challenge.text[i]);
      break;

    case 2: // authorize verify
      if (len==sizeof(authrec.sig)) {
        setAuthorized(false);
        return onSliceBegin(buffer, size, onAuthResponseArrived);
      } else {
        status = 405;
        msg = "Wrong parameter";
      }
      break;

    default:
      status = 405;
      msg = "Wrong parameter";
      break;
    }
    break;

  case REBOOT:
    if (req->p1 == 1) {
      reply(req->ins, status, msg);
      HAL_Delay(100);
      fw_upgrade(0, NULL);
      return 0;
    } else {
      status = 405;
      msg = "Wrong parameter";
    }
    break;

  case WRITE_FIFO:
    return onSliceBegin(buffer, size, onFifoDataArrived);

  case PRODUCT_INFO:
    msg = (uint8_t *)(req+1);
    extern int fw_version(int argc, char *argv[]);
    extern bool isAllZero(uint8_t *s, size_t size);
    sprintf((char *)msg, "{\"serial\":\"%08X\",\"credential\":\"%s\",\"version\":\"%s.%04d\"}",
            bldrcfg.serial,
            isAllZero((uint8_t *)&devKeyCert, sizeof(devKeyCert)) ? "absent" : "present",
            VERSION, BUILDNO);
    break;

  case WRITE_CREDENTIAL:
    switch(req->p1) {
    case 0: // write key
      if (len==sizeof(devKeyCert.key))
        eeprom_copy((uint32_t)devKeyCert.key, (uint32_t)req->payload, len);
      else {
        status = 405;
        msg = "Incorrect length";
      }
      break;

    case 1: // write cert
      if (len%4==0 && len<=sizeof(devKeyCert.cert)) {
        return onSliceBegin(buffer, size, onCertDataReceived);
      } else {
        status = 405;
        msg = "Incorrect length";
      }
      break;

    default:
      status = 405;
      msg = "Wrong parameter";
      break;
    }
    break;

  default:
    DUMP(("Unknown command from usb:", (uint8_t *)req, sizeof(*req)));
    status = 404;
    msg = "Unknown command";
    break;
  }

  reply(req->ins, status, msg);
  return 0;
}

