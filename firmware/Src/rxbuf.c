#include "main.h"

#define RESERVED        0x67A2

#define bufint_t        uint8_t         // handles a buffer <= 256 blocks
//#define bufint_t        uint16_t        // handles a buffer <= 65536 blocks

typedef PACKED_STRUCT {
  bufint_t blksize, total;
  bufint_t head, tail;
  uint8_t buf[1];
} rxbuf_t;

// initialize
void *rxbuf_setup(uint8_t *buf, int buf_size, int seg_size) {
  rxbuf_t *this = (rxbuf_t *)buf;
  if (seg_size%2!=0) { // we'll store RESERVED at the tail of segment block, so we required that it is even
    HardFault_Handler();
  }

  buf_size -= sizeof(rxbuf_t)-1;
  this->total = (bufint_t)(buf_size/seg_size);
  this->blksize = (bufint_t)(buf_size/(int)this->total);
  this->head = this->tail = 0;
  return this;
}

int rxbuf_block_size(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  return (int)this->blksize;
}

int rxbuf_block_number(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  return (int)this->total;
}

/*
 * Reader operations
 */
int rxbuf_is_empty(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  return this->head == this->tail;
}

// get next dirty buffer for read, note that you have to check rxbuf_is_empty() first
uint8_t *rxbuf_get(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  bufint_t head = this->head;
  this->head = (this->head+1)%this->total;
  return this->buf + (uint32_t)this->blksize*(uint32_t)head;
}

/*
 * Writer operations
 */
int rxbuf_is_full(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  return this->head == (this->tail+1)%this->total;
}

int rxbuf_pending_commit(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  uint8_t *blk = this->buf + (uint32_t)this->blksize*(uint32_t)this->tail;
  return *((uint16_t *)(blk+this->blksize)-1)==RESERVED;
}

// get next writeable buffer, note that you have to check rxbuf_is_full() first
bufint_t *rxbuf_reserve(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  uint8_t *blk = this->buf + (uint32_t)this->blksize*(uint32_t)this->tail;
  *((uint16_t *)(blk+this->blksize)-1)=RESERVED;        // write reserved word in the tail
  return blk;
}

void rxbuf_commit(void *h) {
  rxbuf_t *this = (rxbuf_t *)h;
  this->tail = (this->tail+1)%this->total;
}

