/*
 * This file is part of the mghid distribution (https://bitbucket.org/avamagic/mghid).
 * Copyright (c) 2020 Meetingreat Co. Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "usbd_hid.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h> // for tolower()

enum {
  KEYBOARD = 1,
  MOUSE
};

extern USBD_HandleTypeDef hUsbDeviceFS;

/*
 * Mouse Commands
 *
 */

typedef PACKED_STRUCT {
  uint8_t is_dirty;
  uint8_t btn;
  uint16_t x, y;
  uint8_t wheel;
} mouse_t;

static mouse_t mouse = { 0 };

static uint8_t btn_id(const char *labels, char btn) {
  char *p=strchr(labels, tolower(btn));

  return (p==NULL) ? 0 : 1 << (int)(p-labels);
}

int mouse_action() {
  if (!mouse.is_dirty)
    return 0;

  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&mouse, sizeof(mouse));
  mouse.is_dirty = 0;
  return 1;
}

static int mouse_move(int argc, char *argv[]) {
  if (argc<3) return BAD_REQUEST;

  mouse.x = atoi(argv[1]);
  mouse.y = atoi(argv[2]);
  mouse.is_dirty = MOUSE;
  return OK;
}

static int mouse_down(int argc, char *argv[]) {
  if (argc<2) return BAD_REQUEST;

  uint8_t btn = btn_id("lrm", argv[1][0]);
  if (btn==0) // unknown btn
    return UNPROCESSABLE;

  mouse.btn |= btn;
  mouse.is_dirty = MOUSE;
  return OK;
}

static int mouse_up(int argc, char *argv[]) {
  if (argc<2) return BAD_REQUEST;

  uint8_t btn = btn_id("lrm", argv[1][0]);
  if (btn==0)
    return UNPROCESSABLE;

  mouse.btn &= ~btn;
  mouse.is_dirty = MOUSE;
  return OK;
}

static int mouse_scroll(int argc, char *argv[]) {
  if (argc<2) return BAD_REQUEST;

  mouse.wheel = atoi(argv[1]);
  mouse.is_dirty = MOUSE;
  return OK;
}

/*
 * Keyboard Commands
 */

typedef PACKED_STRUCT {
  uint8_t report_id;
  uint8_t modifier;
  uint8_t is_dirty;
  uint8_t keys[6];
} kbd_t;

static kbd_t kbd = { KEYBOARD, 0, 0, {0} };
static uint32_t interval = 50;

int kbd_action() {
  static uint32_t lastupdate;
  uint32_t now = HAL_GetTick();

  if (now >= lastupdate && now-lastupdate<interval && kbd.is_dirty!=1)
    return 0;

  if (kbd.is_dirty==0) {
    // if any keydown
    if (kbd.modifier || kbd.keys[0] || *(uint32_t *)(kbd.keys+1) || kbd.keys[5])
      kbd.is_dirty=1;
    else
      return 0;
  }

  kbd.is_dirty--;
  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&kbd, sizeof(kbd));
  lastupdate = now;
  return 1;
}

static int kbd_repeat_interval(int argc, char *argv[]) {
  if (argc>1) {
    int val;

    sscanf(argv[1], "%d", &val);
    if (val>0) interval = val;
  }

  printf("Keyboard repeat interval = %d ticks.\n", interval);
  return OK;
}

static uint8_t modkey_id(char *s) {
  static const char *labels ="csam";

  return (strncasecmp(s, "right", 5)==0) ? btn_id(labels, s[5]) << 4 :
    (strncasecmp(s, "left", 4)==0) ? btn_id(labels, s[4]): btn_id(labels, s[0]);
}

static int scancode(char *s) {
  int len = strlen(s);
  int ch = 0;
  if (len<1) return 0;

  if (s[len-1]=='h')
    sscanf(s, "%x", &ch);
  else if (s[0]=='0' && s[1]=='x')
    sscanf(s+2, "%x", &ch);
  else
    sscanf(s, "%d", &ch);
  return ch;
}

// find key in kbd.keys[], if found return index+1, or else return empty slot -n-1, return 0 if full and not found
static int key_lookup(int code) {
  int empty=-1;

  for(int i=0;i<LENGTH(kbd.keys);i++) {
    if(empty<0 && kbd.keys[i]==0)
      empty=i;
    else if(kbd.keys[i]==code)
      return i+1;
  }
  return -1-empty;
}

static int kbd_keydown(int argc, char *argv[]) {
  if (argc<2) return BAD_REQUEST;

  // check if ctrl/shift/alt/meta
  int mod = modkey_id(argv[1]);
  if (mod>0) {
    kbd.modifier |= mod;
    kbd.is_dirty = 1;
    return OK;
  }

  // then it's keycode
  int code=scancode(argv[1]);
  if (code==0)
    return UNPROCESSABLE;

  int slot = key_lookup(code);
  if (slot>=0)
    return OK; // full or already down

  kbd.keys[-slot-1]=code;
  kbd.is_dirty = 1;
  return OK;
}

static int kbd_keyup(int argc, char *argv[]) {
  if (argc<2) return BAD_REQUEST;

  // check if ctrl/shift/alt/meta
  int mod = modkey_id(argv[1]);
  if (mod>0) {
    kbd.modifier &= ~mod;
    kbd.is_dirty = 1;
    return OK;
  }

  // then it's keycode
  int code=scancode(argv[1]);
  if (code==0)
    return UNPROCESSABLE;

  int slot = key_lookup(code);
  if (slot<0)
    return OK; // not found

  kbd.keys[slot-1]=0;
  kbd.is_dirty = 1;
  return OK;
}

static int kbd_reset(int argc, char *argv[]) {
  memset(&kbd.keys, 0, sizeof(kbd.keys));
  kbd.modifier = 0;
  kbd.is_dirty = 1;

  mouse.btn = 0;
  return OK;
}

static int dumpbuf(uint8_t *buf, int len) {
  for (int i=0; i<len; i++)
    putchar(buf[i]);

  return len;
}

static int fido_read(int argc, char *argv[]) {
  int len = cycbuf_count(fifoBuf);
  printf("200 %d bytes read.\n", len);
  if (len<1) return 0;

  cycbuf_dump_all(fifoBuf, dumpbuf);
  printf("\n\n");
  return 0;
}


/*
 * Command dispatcher
 */

typedef struct {
  uint16_t code;
  const char *text;
} response_t;

static const char *resp_text(int code) {
  static const response_t tbl[] = {
    { OK, "Ok" },
    { BAD_REQUEST, "Not enough parameters" },
    { UNPROCESSABLE, "Unknown parameters" },
    { NOT_IMPLEMENTED, "Unknown command. Check spelling" }
  };

  for(int i=0;i<LENGTH(tbl);i++) {
    if (code==(int)tbl[i].code)
      return tbl[i].text;
  }
  return "Unknown Error";
}

int help(int argc, char *argv[]) {
  printf(
    "200 --- Mouse Commands ------\n"
    "move\t<x> <y>\n"
    "press\t[left|right|middle]\n"
    "release\t[left|right|middle]\n"
    "------ Keyboard Commands ----\n"
    "down\t[[leftcontrol|rightcontrol|leftalt|rightalt|leftshift|rightshift|leftmeta|rightmeta]\n"
    "down\t<scancode>\n"
    "up\t[leftcontrol|rightcontrol|leftalt|rightalt|leftshift|rightshift|leftmeta|rightmeta]\n"
    "up\t<scancode>\n"
    "interval <ticks>\n"
    "reset\n"
   );

  return 0;
}

int crypto_set_variable(int argc, char *argv[]);
int crypto_generate(int argc, char *argv[]);
int crypto_get_public(int argc, char *argv[]);
int crypto_get_cert(int argc, char *argv[]);
int crypto_sign(int argc, char *argv[]);
int crypto_agree(int argc, char *argv[]);
int crypto_verify(int argc, char *argv[]);

typedef struct {
  const char *cmd;
  int (*handler)(int argc, char *argv[]);
} cmd_handler_t;

extern int fw_version(int, char *argv[]);
extern int fw_upgrade(int, char *argv[]);

static const cmd_handler_t cmd_tbl[] = {
  { "help",    help        },
  { "version", fw_version  },
  { "upgrade", fw_upgrade  },
  // mouse commands
  { "move",    mouse_move  },
  { "press",   mouse_down  },
  { "release", mouse_up    },
  { "btndown", mouse_down  },
  { "btnup",   mouse_up    },
  { "scroll",  mouse_scroll},
  // key commands
  { "down",    kbd_keydown },
  { "up",      kbd_keyup   },
  { "keydown", kbd_keydown },
  { "keyup",   kbd_keyup   },
  { "interval",kbd_repeat_interval },
  { "reset",   kbd_reset   },
  // crypto commands
  { "new",     crypto_generate },
  { "pubkey",  crypto_get_public },
  { "cert",    crypto_get_cert },
  { "sign",    crypto_sign },
  { "agree",   crypto_agree },
  { "set",     crypto_set_variable },
  { "verify",  crypto_verify },
  // proxy commands
  { "read",    fido_read },
  { "readfifo",fido_read },
};

static int parse_command(char *line, char **argv, int max) {
  int i;

  argv[0]=strtok(line, " \t\r\n");
  for(i=1; i<max; i++) {
    argv[i]=strtok(NULL, " \t\r\n");
    if(argv[i]==NULL) break;
  }

  return i;
}

int process_line(char *line) {
  int rt = NOT_IMPLEMENTED;
  char *argv[4];
  int argc;

  if (line==NULL) return 0;

  argc = parse_command(line, (char **)&argv, LENGTH(argv));
  if(argc<1 || *argv[0]==0 || strchr("\r\n/#;", *argv[0])!=NULL)
    return 0;

  for (int i=0; i<LENGTH(cmd_tbl); i++) {
    if (strcasecmp(argv[0], cmd_tbl[i].cmd)==0) {
      rt = cmd_tbl[i].handler(argc, argv);
      break;
    }
  }
  if (rt!=0) printf("%d %s. (%s)\n", rt, resp_text(rt), argv[0]);
  //printf("%d %s. (%s %s)\n", rt, resp_text(rt), argv[0], argc>1 ? argv[1] : "");
  return 0;
}
