/*
 * This file is part of the mghid distribution (https://bitbucket.org/avamagic/mghid).
 * Copyright (c) 2020 Meetingreat Co. Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include "main.h"

#define LENGTH(x) (sizeof(x)/sizeof(*x))

static void uart_init(UART_HandleTypeDef *this, USART_TypeDef *base) {
  this->Instance = base;
  this->Init.BaudRate = 115200;
  this->Init.WordLength = UART_WORDLENGTH_8B;
  this->Init.StopBits = UART_STOPBITS_1;
  this->Init.Parity = UART_PARITY_NONE;
  this->Init.Mode = UART_MODE_TX_RX;
  this->Init.HwFlowCtl = UART_HWCONTROL_NONE;
  this->Init.OverSampling = UART_OVERSAMPLING_16;
  this->Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  this->AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(this) != HAL_OK)
    Error_Handler();

  IRQn_Type irq = (base == USART1) ? USART1_IRQn : (base == USART2) ? USART2_IRQn : (base == USART4) ? USART4_5_IRQn : 0;
  if (irq) {
    NVIC_EnableIRQ(irq);
    NVIC_ClearPendingIRQ(irq);
  }
}

static UART_HandleTypeDef console;

void USART4_5_IRQHandler() {
  HAL_UART_IRQHandler((UART_HandleTypeDef *)&console);
}

void USART2_IRQHandler() {
  HAL_UART_IRQHandler((UART_HandleTypeDef *)&console);
}

void USART1_IRQHandler() {
  HAL_UART_IRQHandler((UART_HandleTypeDef *)&console);
}

void MX_UART_Init() {
  uart_init(&console, USART_CONSOLE);
}

// For printf
int fputc(int ch, FILE *f) {
  HAL_UART_Transmit((UART_HandleTypeDef *)&console, (uint8_t *)&ch, 1, 0xffff);
  return ch;
}

#define ECHO    1

int fgetc(FILE * f) {
  uint8_t ch = 0;

  HAL_UART_Receive((UART_HandleTypeDef *)&console,&ch, 1, 0xffff);
  if (ECHO) printf("%c", ch);
  return ch;
}

char *fgets(char *s, int n, FILE *f) {
  char *p = s;

  for(int ch=1; n>1 && ch!='\r' && ch!='\n'; n--,p++) {
    ch=fgetc(f);
    if(ch==0) break;
    *p=ch;
  }

  if(n>0)*p='\0';
  return p>s ? s : NULL; // if we didn't read any character, return failure
}

int dump(uint8_t *prompt, uint8_t *buf, int len) {
  if (prompt) printf("%s ", prompt);
  for (int i=0; i<len;i++)
    printf("%02x", buf[i]);
  printf("\n");
  return 0;
}

char *readline() {
  static char buf[8][150] = { "" };     // crypto commands needs 10+128 bytes
  static uint8_t onduty = 0;
  UART_HandleTypeDef *huart = (UART_HandleTypeDef *)&console;
  uint8_t dirty;

  // if not yet received complete line, we skip
  if (huart->RxState != HAL_UART_STATE_READY)
    return NULL;
  // we received something
  dirty = onduty;
  onduty = (onduty+1)%LENGTH(buf);

  // retry setting up receive buffer and interrupt until success
  memset(buf[onduty], 0, sizeof(buf[0]));
  while (HAL_UART_Receive_IT(huart, (uint8_t *)buf[onduty], sizeof(buf[0])-1)!=HAL_OK) {
    //printf("Receive error\n");
    HAL_Delay(1);
  }
  // now huart->RxState is set HAL_UART_STATE_BUSY
  return buf[dirty][0] ? buf[dirty] : NULL;
}

