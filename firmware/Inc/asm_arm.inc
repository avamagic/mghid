/* Copyright 2015, Kenneth MacKay. Licensed under the BSD 2-clause license. */
/* Modified by rwlin@meetingreat.com for micro-ecc on IAR/Cortex-M0 */

#ifndef _UECC_ASM_ARM_H_
#define _UECC_ASM_ARM_H_

#define uECC_MIN_WORDS 8                // Caution!! We support secp256r1/k1 only
#define REG_RW "+r"
#define REG_WRITE "=r"
#define REG_RW_LO "+l"
#define REG_WRITE_LO "=l"

uECC_VLI_API uECC_word_t uECC_vli_add(uECC_word_t *result, const uECC_word_t *left, const uECC_word_t *right, wordcount_t num_words) {
    uint32_t carry;
    uint32_t left_word;
    uint32_t right_word;

    asm volatile (
        "movs %[carry], #0 \n\t"

        "ldmia %[lptr]!, {%[left]} \n\t"
        "ldmia %[rptr]!, {%[right]} \n\t"
        "adds %[left], %[left], %[right] \n\t"
        "stmia %[dptr]!, {%[left]} \n\t"

        REPEAT(DEC(uECC_MAX_WORDS),
            "ldmia %[lptr]!, {%[left]} \n\t"
            "ldmia %[rptr]!, {%[right]} \n\t"
            "adcs %[left], %[right] \n\t"
            "stmia %[dptr]!, {%[left]} \n\t")

        "adcs %[carry], %[carry] \n\t"
        : [dptr] REG_RW_LO (result), [lptr] REG_RW_LO (left), [rptr] REG_RW_LO (right),
          [carry] REG_WRITE_LO (carry), [left] REG_WRITE_LO (left_word),
          [right] REG_WRITE_LO (right_word)
        :
        : "cc", "memory"
    );
    return carry;
}
#define asm_add 1

#if !asm_add
uECC_VLI_API uECC_word_t uECC_vli_add(uECC_word_t *result,  const uECC_word_t *left, const uECC_word_t *right, wordcount_t num_words) {
    uint32_t carry = 0;
    uint32_t left_word;
    uint32_t right_word;

    asm volatile (
        "1: \n\t"
        "ldmia %[lptr]!, {%[left]} \n\t"  /* Load left word. */
        "ldmia %[rptr]!, {%[right]} \n\t" /* Load right word. */
        "lsrs %[carry], #1 \n\t"          /* Set up carry flag (carry = 0 after this). */
        "adcs %[left], %[left], %[right] \n\t"   /* Add with carry. */
        "adcs %[carry], %[carry], %[carry] \n\t" /* Store carry bit. */
        "stmia %[dptr]!, {%[left]} \n\t"  /* Store result word. */
        "subs %[ctr], #1 \n\t"            /* Decrement counter. */
        "bne 1b \n\t"                     /* Loop until counter == 0. */
        : [dptr] REG_RW (result), [lptr] REG_RW (left), [rptr] REG_RW (right),
          [ctr] REG_RW (num_words), [carry] REG_RW (carry),
          [left] REG_WRITE (left_word), [right] REG_WRITE (right_word)
        :
        : "cc", "memory"
    );
    return carry;
}
#define asm_add 1
#endif

uECC_VLI_API uECC_word_t uECC_vli_sub(uECC_word_t *result, const uECC_word_t *left, const uECC_word_t *right, wordcount_t num_words) {
    uint32_t carry;
    uint32_t left_word;
    uint32_t right_word;

    asm volatile (
        "movs %[carry], #0 \n\t"

        "ldmia %[lptr]!, {%[left]} \n\t"
        "ldmia %[rptr]!, {%[right]} \n\t"
        "subs %[left], %[left], %[right] \n\t"
        "stmia %[dptr]!, {%[left]} \n\t"

        REPEAT(DEC(uECC_MAX_WORDS),
            "ldmia %[lptr]!, {%[left]} \n\t"
            "ldmia %[rptr]!, {%[right]} \n\t"
            "sbcs %[left], %[right] \n\t"
            "stmia %[dptr]!, {%[left]} \n\t")

        "adcs %[carry], %[carry] \n\t"
        : [dptr] REG_RW_LO (result), [lptr] REG_RW_LO (left), [rptr] REG_RW_LO (right),
          [carry] REG_WRITE_LO (carry), [left] REG_WRITE_LO (left_word),
          [right] REG_WRITE_LO (right_word)
        :
        : "cc", "memory"
    );
    return !carry; /* Note that on ARM, carry flag set means "no borrow" when subtracting
                      (for some reason...) */
}
#define asm_sub 1

#if !asm_sub
uECC_VLI_API uECC_word_t uECC_vli_sub(uECC_word_t *result, const uECC_word_t *left, const uECC_word_t *right, wordcount_t num_words) {
    uint32_t carry = 1; /* carry = 1 initially (means don't borrow) */
    uint32_t left_word;
    uint32_t right_word;

    asm volatile (
        "1: \n\t"
        "ldmia %[lptr]!, {%[left]} \n\t"  /* Load left word. */
        "ldmia %[rptr]!, {%[right]} \n\t" /* Load right word. */
        "lsrs %[carry], #1 \n\t"          /* Set up carry flag (carry = 0 after this). */
        "sbcs %[left], %[left], %[right] \n\t"   /* Subtract with borrow. */
        "adcs %[carry], %[carry], %[carry] \n\t" /* Store carry bit. */
        "stmia %[dptr]!, {%[left]} \n\t"  /* Store result word. */
        "subs %[ctr], #1 \n\t"            /* Decrement counter. */
        "bne 1b \n\t"                     /* Loop until counter == 0. */
        : [dptr] REG_RW (result), [lptr] REG_RW (left), [rptr] REG_RW (right),
          [ctr] REG_RW (num_words), [carry] REG_RW (carry),
          [left] REG_WRITE (left_word), [right] REG_WRITE (right_word)
        :
        : "cc", "memory"
    );
    return !carry;
}
#define asm_sub 1
#endif

#if !asm_mult
uECC_VLI_API void uECC_vli_mult(uECC_word_t *result, const uECC_word_t *left, const uECC_word_t *right, wordcount_t num_words) {
    uint32_t r4, r5, r6, r7;

    asm volatile (
        "subs %[r3], #1 \n\t" /* r3 = num_words - 1 */
        "lsls %[r3], #2 \n\t" /* r3 = (num_words - 1) * 4 */
        "mov r8, %[r3] \n\t"  /* r8 = (num_words - 1) * 4 */
        "lsls %[r3], #1 \n\t" /* r3 = (num_words - 1) * 8 */
        "mov r9, %[r3] \n\t"  /* r9 = (num_words - 1) * 8 */
        "movs %[r3], #0 \n\t" /* c0 = 0 */
        "movs %[r4], #0 \n\t" /* c1 = 0 */
        "movs %[r5], #0 \n\t" /* c2 = 0 */
        "movs %[r6], #0 \n\t" /* k = 0 */

        "push {%[r0]} \n" /* keep result on the stack */

        "1: \n\t" /* outer loop (k < num_words) */
        "movs %[r7], #0 \n\t" /* r7 = i = 0 */
        "b 3f \n"

        "2: \n\t" /* outer loop (k >= num_words) */
        "movs %[r7], %[r6] \n\t" /* r7 = k */
        "mov %[r0], r8 \n\t"     /* r0 = (num_words - 1) * 4 */
        "subs %[r7], %[r7], %[r0] \n" /* r7 = i = k - (num_words - 1) (times 4) */

        "3: \n\t" /* inner loop */
        "mov r10, %[r3] \n\t"
        "mov r11, %[r4] \n\t"
        "mov r12, %[r5] \n\t"
        "mov r14, %[r6] \n\t"
        "subs %[r0], %[r6], %[r7] \n\t"          /* r0 = k - i */

        "ldr %[r4], [%[r2], %[r0]] \n\t" /* r4 = right[k - i] */
        "ldr %[r0], [%[r1], %[r7]] \n\t" /* r0 = left[i] */

        "lsrs %[r3], %[r0], #16 \n\t" /* r3 = a1 */
        "uxth %[r0], %[r0] \n\t"      /* r0 = a0 */

        "lsrs %[r5], %[r4], #16 \n\t" /* r5 = b1 */
        "uxth %[r4], %[r4] \n\t"      /* r4 = b0 */

        "movs %[r6], %[r3] \n\t"        /* r6 = a1 */
        "muls %[r6], %[r5], %[r6] \n\t" /* r6 = a1 * b1 */
        "muls %[r3], %[r4], %[r3] \n\t" /* r3 = b0 * a1 */
        "muls %[r5], %[r0], %[r5] \n\t" /* r5 = a0 * b1 */
        "muls %[r0], %[r4], %[r0] \n\t" /* r0 = a0 * b0 */

        /* Add middle terms */
        "lsls %[r4], %[r3], #16 \n\t"
        "lsrs %[r3], %[r3], #16 \n\t"
        "adds %[r0], %[r0], %[r4] \n\t"
        "adcs %[r6], %[r3] \n\t"

        "lsls %[r4], %[r5], #16 \n\t"
        "lsrs %[r5], %[r5], #16 \n\t"
        "adds %[r0], %[r0], %[r4] \n\t"
        "adcs %[r6], %[r5] \n\t"

        "mov %[r3], r10\n\t"
        "mov %[r4], r11\n\t"
        "mov %[r5], r12\n\t"
        "adds %[r3], %[r3], %[r0] \n\t"         /* add low word to c0 */
        "adcs %[r4], %[r6] \n\t"         /* add high word to c1, including carry */
        "movs %[r0], #0 \n\t"            /* r0 = 0 (does not affect carry bit) */
        "adcs %[r5], %[r0] \n\t"         /* add carry to c2 */

        "mov %[r6], r14\n\t" /* r6 = k */

        "adds %[r7], #4 \n\t"   /* i += 4 */
        "cmp %[r7], r8 \n\t"    /* i > (num_words - 1) (times 4)? */
        "bgt 4f \n\t"           /*   if so, exit the loop */
        "cmp %[r7], %[r6] \n\t" /* i <= k? */
        "ble 3b \n"           /*   if so, continue looping */

        "4: \n\t" /* end inner loop */

        "ldr %[r0], [sp, #0] \n\t" /* r0 = result */

        "str %[r3], [%[r0], %[r6]] \n\t" /* result[k] = c0 */
        "mov %[r3], %[r4] \n\t"          /* c0 = c1 */
        "mov %[r4], %[r5] \n\t"          /* c1 = c2 */
        "movs %[r5], #0 \n\t"            /* c2 = 0 */
        "adds %[r6], #4 \n\t"            /* k += 4 */
        "cmp %[r6], r8 \n\t"             /* k <= (num_words - 1) (times 4) ? */
        "ble 1b \n\t"                    /*   if so, loop back, start with i = 0 */
        "cmp %[r6], r9 \n\t"             /* k <= (num_words * 2 - 2) (times 4) ? */
        "ble 2b \n\t"                    /*   if so, loop back, with i = (k + 1) - num_words */
        /* end outer loop */

        "str %[r3], [%[r0], %[r6]] \n\t" /* result[num_words * 2 - 1] = c0 */
        "pop {%[r0]} \n\t"               /* pop result off the stack */

        : [r3] "+l" (num_words), [r4] "=&l" (r4),
          [r5] "=&l" (r5), [r6] "=&l" (r6), [r7] "=&l" (r7)
        : [r0] "l" (result), [r1] "l" (left), [r2] "l" (right)
        : "r12", "r14", "cc", "memory"
        // : "r8", "r9", "r10", "r11", "r12", "r14", "cc", "memory"
    );
#endif
}
#define asm_mult 1

#if !asm_square
uECC_VLI_API void uECC_vli_square(uECC_word_t *result, const uECC_word_t *left, wordcount_t num_words) {
   uint32_t r3, r4, r5, r6, r7;

   asm volatile (
        "subs %[r2], #1 \n\t" /* r2 = num_words - 1 */
        "lsls %[r2], #2 \n\t" /* r2 = (num_words - 1) * 4 */
        "mov r8, %[r2] \n\t"  /* r8 = (num_words - 1) * 4 */
        "lsls %[r2], #1 \n\t" /* r2 = (num_words - 1) * 8 */
        "mov r9, %[r2] \n\t"  /* r9 = (num_words - 1) * 8 */
        "movs %[r2], #0 \n\t" /* c0 = 0 */
        "movs %[r3], #0 \n\t" /* c1 = 0 */
        "movs %[r4], #0 \n\t" /* c2 = 0 */
        "movs %[r5], #0 \n\t" /* k = 0 */

        "push {%[r0]} \n" /* keep result on the stack */

        "1: \n\t" /* outer loop (k < num_words) */
        "movs %[r6], #0 \n\t" /* r6 = i = 0 */
        "b 3f \n"

        "2: \n\t" /* outer loop (k >= num_words) */
        "movs %[r6], %[r5] \n\t" /* r6 = k */
        "mov %[r0], r8 \n\t"     /* r0 = (num_words - 1) * 4 */
        "subs %[r6], %[r6], %[r0] \n" /* r6 = i = k - (num_words - 1) (times 4) */

        "3: \n" /* inner loop */
        "mov r10, %[r2] \n\t"
        "mov r11, %[r3] \n\t"
        "mov r12, %[r4] \n\t"
        "mov r14, %[r5] \n\t"
        "subs %[r7], %[r5], %[r6] \n\t"  /* r7 = k - i */

        "ldr %[r3], [%[r1], %[r7]] \n\t" /* r3 = left[k - i] */
        "ldr %[r0], [%[r1], %[r6]] \n\t" /* r0 = left[i] */

        "lsrs %[r2], %[r0], #16 \n\t" /* r2 = a1 */
        "uxth %[r0], %[r0] \n\t"      /* r0 = a0 */

        "lsrs %[r4], %[r3], #16 \n\t" /* r4 = b1 */
        "uxth %[r3], %[r3] \n\t"      /* r3 = b0 */

        "movs %[r5], %[r2] \n\t"        /* r5 = a1 */
        "muls %[r5], %[r4], %[r5] \n\t" /* r5 = a1 * b1 */
        "muls %[r2], %[r3], %[r2] \n\t" /* r2 = b0 * a1 */
        "muls %[r4], %[r0], %[r4] \n\t" /* r4 = a0 * b1 */
        "muls %[r0], %[r3], %[r0] \n\t" /* r0 = a0 * b0 */

        /* Add middle terms */
        "lsls %[r3], %[r2], #16 \n\t"
        "lsrs %[r2], %[r2], #16 \n\t"
        "adds %[r0], %[r0], %[r3] \n\t"
        "adcs %[r5], %[r2] \n\t"

        "lsls %[r3], %[r4], #16 \n\t"
        "lsrs %[r4], %[r4], #16 \n\t"
        "adds %[r0], %[r0], %[r3] \n\t"
        "adcs %[r5], %[r4] \n\t"

        /* Add to acc, doubling if necessary */
        "mov %[r2], r10\n\t"
        "mov %[r3], r11\n\t"
        "mov %[r4], r12\n\t"

        "cmp %[r6], %[r7] \n\t"    /* (i < k - i) ? */
        "bge 4f \n\t"            /*   if i >= k - i, skip */
        "movs %[r7], #0 \n\t"    /* r7 = 0 */
        "adds %[r2], %[r2], %[r0] \n\t" /* add low word to c0 */
        "adcs %[r3], %[r5] \n\t" /* add high word to c1, including carry */
        "adcs %[r4], %[r7] \n" /* add carry to c2 */
        "4: \n\t"
        "movs %[r7], #0 \n\t"    /* r7 = 0 */
        "adds %[r2], %[r2], %[r0] \n\t" /* add low word to c0 */
        "adcs %[r3], %[r5] \n\t" /* add high word to c1, including carry */
        "adcs %[r4], %[r7] \n\t" /* add carry to c2 */

        "mov %[r5], r14\n\t" /* r5 = k */

        "adds %[r6], #4 \n\t"           /* i += 4 */
        "cmp %[r6], %[r5] \n\t"         /* i >= k? */
        "bge 5f \n\t"                   /*   if so, exit the loop */
        "subs %[r7], %[r5], %[r6] \n\t" /* r7 = k - i */
        "cmp %[r6], %[r7] \n\t"         /* i <= k - i? */
        "ble 3b \n"                   /*   if so, continue looping */

        "5: \n\t" /* end inner loop */

        "ldr %[r0], [sp, #0] \n\t" /* r0 = result */

        "str %[r2], [%[r0], %[r5]] \n\t" /* result[k] = c0 */
        "mov %[r2], %[r3] \n\t"          /* c0 = c1 */
        "mov %[r3], %[r4] \n\t"          /* c1 = c2 */
        "movs %[r4], #0 \n\t"            /* c2 = 0 */
        "adds %[r5], #4 \n\t"            /* k += 4 */
        "cmp %[r5], r8 \n\t"             /* k <= (num_words - 1) (times 4) ? */
        "ble 1b \n\t"                    /*   if so, loop back, start with i = 0 */
        "cmp %[r5], r9 \n\t"             /* k <= (num_words * 2 - 2) (times 4) ? */
        "ble 2b \n\t"                    /*   if so, loop back, with i = (k + 1) - num_words */
        /* end outer loop */

        "str %[r2], [%[r0], %[r5]] \n\t" /* result[num_words * 2 - 1] = c0 */
        "pop {%[r0]} \n\t"               /* pop result off the stack */

        : [r2] "+l" (num_words), [r3] "=&l" (r3), [r4] "=&l" (r4),
          [r5] "=&l" (r5), [r6] "=&l" (r6), [r7] "=&l" (r7)
        : [r0] "l" (result), [r1] "l" (left)
        : "r12", "r14", "cc", "memory"
    );
}
#define asm_square 1
#endif

#endif /* _UECC_ASM_ARM_H_ */
