/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2020 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "config.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define VENDOR_ID       0x1B80
#define VENDOR_NAME     "Meetingreat"
#define PRODUCT_ID      0xE106

#define SystemClock_Config() SystemClock_Config_Mgfido()
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#ifdef __IAR_SYSTEMS_ICC__
#define PACKED_STRUCT __packed struct
#else
#define PACKED_STRUCT struct __attribute__((packed))
#endif

#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH

#ifdef HW_AMEBA
  #define USART_CONSOLE USART1
#else
  #define USART_CONSOLE USART2
#endif

#define LED1_Pin GPIO_PIN_5
#define LED1_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA

/* USER CODE BEGIN Private defines */

#define LENGTH(x) (sizeof(x)/sizeof(*x))

typedef PACKED_STRUCT {
  uint32_t      magic;
  uint32_t      download_mode;
  uint32_t      serial;
} bootldr_cfg_t;

__no_init bootldr_cfg_t bldrcfg @ DATA_EEPROM_BASE; // 0x08080000

typedef PACKED_STRUCT {
  uint8_t       key[32];
  uint8_t       cert[256-32];
} device_keycert_t;

__no_init device_keycert_t devKeyCert @ (DATA_EEPROM_BANK2_END+1-sizeof(device_keycert_t)); // 256B from the bottom

enum {
  OK                    = 200,
  BAD_REQUEST           = 400,
  UNPROCESSABLE         = 422,
  NOT_IMPLEMENTED       = 501
};

#define min(x, y)       ((x)<=(y) ? (x) : (y))

void *rxbuf_setup(uint8_t *buf, int buf_size, int seg_size); // note: seg_size MUST be even
int rxbuf_block_size(void *h);
int rxbuf_block_number(void *h);
int rxbuf_is_empty(void *h);
int rxbuf_is_full(void *h);
uint8_t *rxbuf_get(void *h);
uint8_t *rxbuf_reserve(void *h);
void rxbuf_commit(void *h);
int rxbuf_pending_commit(void *h);

void *cycbuf_setup(uint8_t *buffer, int size);
int cycbuf_size(void *h);
int cycbuf_count(void *h);
int cycbuf_is_empty(void *h);
int cycbuf_is_full(void *h);
int cycbuf_write(void *h, void *buf, int size);
int cycbuf_read(void *h, void *buf, int size);
void cycbuf_dump_all(void *h, int (*dump_block)(uint8_t *src, int n));

int dump(uint8_t *prompt, uint8_t *buf, int len);

extern void *hidBuf, *fifoBuf;

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
