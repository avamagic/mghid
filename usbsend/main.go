package main

import (
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/flynn/hid"
)

const (
	COMMAND_CHANNEL = 3
	WRITE_FIFO      = 4
)

func prepare(b []byte, ins, p1, p2 byte, size int) {
	b[0] = COMMAND_CHANNEL
	b[1] = 0
	b[2] = ins
	b[3] = p1
	b[4] = p2
	b[7] = byte(size & 0xFF)
	b[6] = byte((size >> 8) & 0xFF)
	b[5] = byte((size >> 16) & 0xFF)
}

func min(x, y int) int {
	if x <= y {
		return x
	} else {
		return y
	}
}

func main() {
	log.SetFlags(0)

	if len(os.Args) < 2 {
		log.Printf("Usage: %s <filename>", os.Args[0])
		return
	}
	payload, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatalf("Failed to open %s: %v\n", os.Args[1], err)
	}

	devices, err := hid.Devices()
	if err != nil {
		log.Fatalf("No HID devices connected or usb error. (%v)\n", err)
	}

	var di *hid.DeviceInfo
	for _, d := range devices {
		if d.VendorID == 0x1B80 && d.ProductID == 0xE106 && d.UsagePage == 0x06 && d.Usage == 0x01 {
			log.Printf("Found device: %04x:%04x/%04x/%04x", d.VendorID, d.ProductID, d.UsagePage, d.Usage)
			di = d
		}
	}
	if di == nil {
		log.Fatalf("Device not found.\n")
	}

	dev, err := di.Open()
	if err != nil {
		log.Fatalf("Failed to open device: %v\n", err)
	}
	defer dev.Close()

	// prepare request
	req := make([]byte, di.OutputReportLength+1) // 1 byte for channel #
	remains := len(payload)
	prepare(req, WRITE_FIFO, 0, 0, remains)

	// send request
	sent := false
	blk := len(req) - 8
	n := min(remains, blk)
	for remains > 0 || !sent {
		copy(req[8:], payload[:n])
		if err = dev.Write(req); err != nil {
			log.Fatalf("Error sending command: %v.\n", err)
			return
		}
		time.Sleep(100 * time.Microsecond)
		sent = true
		remains -= n
		if remains > 0 {
			payload = payload[n:]
			n = min(remains, blk)
			prepare(req, 0, 0, 0, n)
		}
	}

	// Read response
	input := dev.ReadCh()
	resp := <-input
	if len(resp) < 8 || resp[0] != COMMAND_CHANNEL {
		log.Fatalf("Response channel error")
	}
	code := uint16(resp[3])<<8 | uint16(resp[4])
	size := uint16(resp[6])<<8 | uint16(resp[7])
	msg := resp[8:]
	if len(msg) > int(size) {
		msg = msg[:size]
	}

	log.Printf("%03d %s\n", code, string(msg))
}
