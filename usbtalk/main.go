package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/flynn/hid"
)

const (
	HID_BLOCK_SIZE       = 64
	CMD_PRODUCT_INFO     = 1
	CMD_PROTOCOL_VERSION = 2
	CMD_REBOOT           = 3
	CMD_WRITE_FIFO       = 4
)

func APDU(class, ins, p1, p2 byte, size int) []byte {
	b := make([]byte, 7+size)
	b[0] = class
	b[1] = ins
	b[2] = p1
	b[3] = p2
	b[6] = byte(size & 0xFF)
	b[5] = byte((size >> 8) & 0xFF)
	b[4] = byte((size >> 16) & 0xFF)
	return b
}

func Scanln(reader *bufio.Reader, prompt string) []string {
	if prompt != "" {
		fmt.Printf("%s", prompt)
	}

	cmdline, err := reader.ReadString('\n')
	if err != nil {
		return nil
	}
	cmdline = strings.TrimSpace(cmdline)
	tokens := strings.Split(cmdline, " ")
	all := make([]string, len(tokens)+1)
	all[0] = cmdline
	copy(all[1:], tokens)
	return all
}

func main() {
	devices, err := hid.Devices()
	if err != nil {
		log.Printf("Error: %v\n", err)
		return
	}

	var di *hid.DeviceInfo
	for _, d := range devices {
		log.Printf("Detected [%04x:%04x] Usage %04x ...", d.VendorID, d.ProductID, d.Usage)
		if d.ProductID == 0xE106 {
			//if d.VendorID == 0x483 && d.ProductID == 0xF1C0 && d.UsagePage == 0x06 {
			log.Printf("Found %+v\n", d)
			di = d
		}
	}
	if di == nil {
		log.Fatalf("Device not found.\n")
	}

	dev, err := di.Open()
	if err != nil {
		log.Fatalf("Failed to open device: %v\n", err)
	}

	console := bufio.NewReader(os.Stdin)
	b := make([]byte, HID_BLOCK_SIZE)
	b[0] = 3
	for {
		argv := Scanln(console, fmt.Sprintf("\n%s# ", di.Product))
		if argv == nil {
			break
		} else if len(argv) < 2 || len(argv[1]) < 1 || argv[1][0] == '#' || argv[1][0] == '/' {
			continue
		}

		switch argv[1] {
		case "quit", "q", "exit", "bye":
			dev.Close()
			fmt.Println("Goodbye.")
			return

		case "upgrade":
			copy(b[1:], APDU(0, CMD_REBOOT, 1, 0, 0))

		case "w":
			if len(argv) < 3 {
				continue
			}
			src := []byte(argv[2])
			remains := len(src)
			copy(b[1:], APDU(0, CMD_WRITE_FIFO, 0, 0, remains))

			for offset := 8; remains > 0; offset = 1 {
				n := HID_BLOCK_SIZE - offset
				if remains < n {
					n = remains
				}
				copy(b[offset:], src[:n])
				if err := dev.Write(b); err != nil {
					fmt.Println(err)
					break
				}
				remains -= n
				src = src[n:]
			}
			if remains == 0 {
				fmt.Println("Ok.")
			}
			continue

		default:
			continue
		}

		if err := dev.Write(b); err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Ok.")
		}
	}

	dev.Close()
}
