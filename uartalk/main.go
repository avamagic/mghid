package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/tarm/serial"
)

func Scanln(reader *bufio.Reader, prompt string) []string {
	if prompt != "" {
		fmt.Printf("%s", prompt)
	}

	cmdline, err := reader.ReadString('\n')
	if err != nil {
		return nil
	}
	cmdline = strings.TrimSpace(cmdline)
	tokens := strings.Split(cmdline, " ")
	all := make([]string, len(tokens)+1)
	all[0] = cmdline
	copy(all[1:], tokens)
	return all
}

func dumpResponse(s io.Reader) {
	buf := make([]byte, 1)
	// read out all responses
	for {
		if n, err := s.Read(buf); err != nil || n < 1 {
			return
		}
		fmt.Printf("%c", buf[0])
	}
}

func main() {
	burst := flag.Bool("b", false, "Dont wait for response")
	timeout := flag.Int("t", 10, "Read timeout in millisecond")
	flag.Parse()

	log.SetFlags(0)

	if flag.NArg() < 1 {
		log.Fatalf("Usage: %s <UART name>\n", os.Args[0])
		return
	}
	name := flag.Arg(0)
	fmt.Printf("Open %s ... ", name)

	out, err := serial.OpenPort(&serial.Config{Name: name, Baud: 115200, ReadTimeout: time.Millisecond * time.Duration(*timeout)})
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	fmt.Printf("Ok\n")

	console := bufio.NewReader(os.Stdin)
	for {
		argv := Scanln(console, fmt.Sprintf("\n%s# ", name))
		if argv == nil {
			break
		} else if len(argv) < 2 || len(argv[1]) < 1 || argv[1][0] == '#' || argv[1][0] == '/' {
			continue
		}

		switch argv[1] {
		case "quit", "q", "exit", "bye":
			fmt.Println("Goodbye.")
			return

		case "sleep", "wait":
			t := 0
			if n, err := fmt.Sscanf(argv[2], "%d", &t); err == nil && n == 1 && t > 0 {
				fmt.Printf("200 Wait for %d second.\n", t)
				time.Sleep(time.Duration(t) * time.Second)
			}

		case "msleep", "mwait":
			t := 0
			if n, err := fmt.Sscanf(argv[2], "%d", &t); err == nil && n == 1 && t > 0 {
				fmt.Printf("200 Wait for %d ms.\n", t)
				time.Sleep(time.Duration(t) * time.Millisecond)
			}

		default:
			if _, err := out.Write([]byte(fmt.Sprintf("%s\n", argv[0]))); err != nil {
				log.Printf("Write error: %v\n", err)
				continue
			}

			if !*burst {
				dumpResponse(out)
			}
		}
	}

	if *burst {
		dumpResponse(out)
	}
}
